//Jedan Garcia
//997358166

// Joseph Cranmer
// 996914530

import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class TeamJ {
	// Global class
	// variables-------------------------------------------------------
	// Output for males is 1, for females 0
	public static final double FEMALE = 1;
	public static final double MALE = 0;

	// size of a scanned picture input
	public static final int SIZE_OF_PICTURE = 128 * 120;
	public static final int PICTURE_WIDTH = 128;

	// Network layers, in this case 5 hidden and 1 output
	// additional layers can be added, for example {25, 10, 1} is valid
	// 
	public static final int[] layers = { 5,  1 };

	// This contains all the Neurons needed
	public static Neuron[][] neuralNetwork = new Neuron[layers.length][layers[0]];

	public static double LEARNING_STEP = 0.1;

	// different configurations for different command line param.
	public static final int TRAINING = 0;
	public static final int TESTING = 1;

	// Mode for application is default to training
	public static int runningMode = TRAINING;

	// How many rounds it should run
	public static int roundNumber = 1;

	// These hold the training and the test sets of pictures
	public static ArrayList<Picture> trainingSet = new ArrayList<Picture>();
	public static ArrayList<Picture> testSet = new ArrayList<Picture>();
	
	public static boolean debug = false;
	public static boolean printWeightImages = false;

	// Main-------------------------------------------------------------------
	public static void main(String[] args) {

		if (args.length < 1) {
			System.out
					.println("This program requires one command line parameter: ");
			System.out.println("\t\t\t\t-train");
			System.out.println("\t\t\t\t-test\n");
			System.exit(0);
		}

		// Setup neurons
		init();

		// Set up the config according to command line param.
		config(args);

		// fill in the training and test sets of pictures
		fillSetsWithPictures();

		// Actually runs the program
		runrounds();

		System.out.println();
	}// main

	// Actual function that runs the rounds
	public static void runrounds(){
		
		// let's keep track of the training/test averages and their std dev
		double totalTrainingPercent = 0;
		double totalCVPercent = 0;
		double trainingPercents[] = new double[roundNumber+1];
		double cvPercents[] = new double[roundNumber+1];
		double totalConfidence = 0;
		int numTests = 0;

		for (int roundCounter = 1; roundCounter <= roundNumber; roundCounter++) {

			totalConfidence = 0;
			numTests = 0;


			Collections.shuffle(trainingSet);
			double numberOfCorrectMales = 0, numberOfCorrectFemales = 0, totalMales = 0, totalFemales = 0;
			
			//System.out.println("\n\n size of trainingSet: " + trainingSet.size() + "\n\n");
			
			for (int i = 0; i < trainingSet.size(); i++) {
				Picture trainingImage = trainingSet.get(i);
				double ourPrediction = useFileToTrain(trainingImage.data, trainingImage.gender);
		
				if (trainingImage.gender == MALE)
					totalMales++;
				if (trainingImage.gender == FEMALE)
					totalFemales++;
				if (trainingImage.gender == MALE && ourPrediction <= 0.5)
					numberOfCorrectMales++;
				if (trainingImage.gender == FEMALE && ourPrediction > 0.5)
					numberOfCorrectFemales++;

				if(debug)
					System.out.println(trainingImage.fileName + ":\t" + trainingImage.gender
							+ " == " + ourPrediction);
			}
			
			double totalRatio = (((numberOfCorrectMales + numberOfCorrectFemales) / (totalMales + totalFemales)) * 100);



			System.out.format("round %2d: Training Results [Male: %3d/%3d (%3d%%)  |  Female: %3d/%3d (%3d%%)  |  Total: %3d/%3d (%3d%%)] \r\n", roundCounter, 
								(int)numberOfCorrectMales, (int)totalMales, (int)(numberOfCorrectMales/totalMales * 100.0),
								(int)numberOfCorrectFemales, (int)totalFemales, (int)(numberOfCorrectFemales/totalFemales * 100.0),
								(int)(numberOfCorrectMales+numberOfCorrectFemales), (int)(totalMales+totalFemales), (int)totalRatio);
			
			// Add that total ratio to our counter
			totalTrainingPercent += totalRatio;
			trainingPercents[roundCounter] = totalRatio;

			//reseting the variables so they can be used for the test set
			numberOfCorrectMales = numberOfCorrectFemales = 0;
			totalMales = totalFemales = 0;

			
			for (int i = 0; i < testSet.size(); i++) {
				Picture testImage = testSet.get(i);
				
				double prediction = runTest(testImage.data);
				
				if (testImage.gender == MALE)
					totalMales++;
				if (testImage.gender == FEMALE)
					totalFemales++;
				if (testImage.gender == MALE && prediction <= 0.5)
					numberOfCorrectMales++;
				if (testImage.gender == FEMALE && prediction > 0.5)
					numberOfCorrectFemales++;
				
				if(runningMode == TESTING){
					String predicted = prediction <= 0.5? "MALE" : "FEMALE";
//					System.out.println(predicted);
					System.out.println(testImage.fileName + ":\t" + predicted);

					if (prediction > 0.5)
					{
						// confidence factor = 1 - prediction
						totalConfidence += (1-prediction);
					}
					else
					{
						// confidence factor = prediction
						totalConfidence += prediction;
					}
					numTests++;
				}
			}

			totalRatio = ((numberOfCorrectMales + numberOfCorrectFemales) / (totalMales + totalFemales) * 100.0);
			
			if(runningMode == TRAINING){
				System.out.format("          Cross Validation [Male: %3d/%3d (%3d%%)  |  Female: %3d/%3d (%3d%%)  |  Total: %3d/%3d (%3d%%)] \r\n\r\n\r\n", 
					(int)numberOfCorrectMales, (int)totalMales, (int)(numberOfCorrectMales/totalMales * 100.0),
					(int)numberOfCorrectFemales, (int)totalFemales, (int)(numberOfCorrectFemales/totalFemales * 100.0),
					(int)(numberOfCorrectMales+numberOfCorrectFemales), (int)(totalMales+totalFemales), (int)totalRatio);
			
			LEARNING_STEP = LEARNING_STEP < 0.1? 0.1 : LEARNING_STEP * .98;
			}

			totalCVPercent += totalRatio;
			cvPercents[roundCounter] = totalRatio;
		
				if (runningMode == TESTING)
				{
					double confidenceFactor = (1-(totalConfidence / numTests))*100;
					System.out.format("Confidence for this round's order: %3.2f%% \r\n", confidenceFactor);
				}

		}//for (roundNumber) # rounds

		double avgTotalTrainingPercent = totalTrainingPercent/roundNumber;
		double avgTotalCVPercent = totalCVPercent/roundNumber;

		// get standard deviation for the above numbers
		//
		double stdDevTraining = 0;
		double stdDevCV = 0;
		for (int i = 1; i <= roundNumber; i++)
		{
			stdDevTraining += Math.pow((trainingPercents[i] - avgTotalTrainingPercent),2)/roundNumber;
			stdDevCV += Math.pow((cvPercents[i] - avgTotalCVPercent),2)/roundNumber;
		}

		stdDevTraining = Math.sqrt(stdDevTraining);
		stdDevCV = Math.sqrt(stdDevCV);

		System.out.format("-----> Rounds complete. Average Training Accuracy: %3.2f%% +/- %3.2f%% Avg CV Accuracy: %3.2f%% +/- %3.2f%% \r\n", avgTotalTrainingPercent, stdDevTraining, avgTotalCVPercent, stdDevCV);

		printWeights();
	}
	
	//run a test-----------------------------------------------
	public static double runTest(int[] data){
		
		//Put the data into the bottom=most layer
		for (int i = 0; i < SIZE_OF_PICTURE; i++) {
			for (int j = 0; j < layers[0]; j++) {
				neuralNetwork[0][j].incoming_input[i] = data[i];
			}
		}
		
		//Calculate the ouputs from neural network
		for (int i = 0; i < layers.length; i++) {
			for (int j = 0; j < layers[i]; j++) {
				neuralNetwork[i][j].setupInputs();
				neuralNetwork[i][j].calculateOutput();
			}
		}
		
		return neuralNetwork[layers.length - 1][0].output;
	}

	// Filling in the training and test sets with pictures-----------------------------------
	public static void fillSetsWithPictures() {
		
		// Loading up the trainingSet with male pictures
		ArrayList<String> trainMale = getFiles("./Male/");
		//System.out.println("Number of files found under male training: " + trainMale.size() + "\n\n");
		for (int i = 0; i < trainMale.size(); i++) {
			String filename = trainMale.get(i);
			trainingSet.add(new Picture(filename, MALE));
		}
		
		// Loading up the trainingSet with male pictures
		ArrayList<String> trainFemale = getFiles("./Female/");
		for (int i = 0; i < trainFemale.size(); i++) {
			String filename = trainFemale.get(i);
			trainingSet.add(new Picture(filename, FEMALE));
		}

		if (runningMode == TESTING) {
			// Loading up testingSet
			ArrayList<String> testPictures = getFiles("./Test/");
			for (int i = 0; i < testPictures.size(); i++) {
				String fileName = testPictures.get(i);
				testSet.add(new Picture(fileName, MALE));
			}
		}// testing

		if (runningMode == TRAINING) {
			Collections.shuffle(trainingSet, new Random(808));

			// For 5-Fold Cross validation we need to seperate one of
			// the training data into the test data randomly
			testSet = new ArrayList<Picture>();
			int randomCrossValidationDivider = 5;
			
			ArrayList<Picture> tempTrainingSet = new ArrayList<Picture>();
			for (int i = 0; i < trainingSet.size(); i++) {
				
				if ( (i % randomCrossValidationDivider) == 0)
					testSet.add(trainingSet.get(i));
				else
					// put them into the regular training bunch
					tempTrainingSet.add(trainingSet.get(i));
			}
			trainingSet = tempTrainingSet;
		}// Training

	}

	// Get All .txt Files in a directory--------------------------------------
	public static ArrayList<String> getFiles(String path) {

		ArrayList<String> files = new ArrayList<String>();
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				if (listOfFiles[i].getName().endsWith(".txt")
						|| listOfFiles[i].getName().endsWith(".TXT")) {

					files.add(path + listOfFiles[i].getName());
				}
			}
		}
		return files;
	}

	// Configure command line Parameter Options-----------------------
	public static void config(String[] args) {

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-train"))
				runningMode = TRAINING;
			if(args[i].equals("-debug"))
				debug = true;
			if (args[i].equals("-test"))
				runningMode = TESTING;

			if (args[i].equals("-rounds")) {
				roundNumber = Integer.parseInt(args[i + 1]);
				i++;
			}
			if (args[i].equals("-print"))
			{
				printWeightImages = true;
			}
		}
	}

	// Initializing------------------------------------------------------------
	public static void init() {
		for (int layer_level = 0; layer_level < layers.length; layer_level++) {
			for (int neuron_it = 0; neuron_it < layers[layer_level]; neuron_it++) {

				int number_of_weights = (layer_level == 0) ? SIZE_OF_PICTURE
						: layers[layer_level - 1];
				int number_of_previous_neurons = (layer_level > 0) ? layers[layer_level - 1]
						: 0;
				int number_of_next_neurons = (layer_level < layers.length - 1) ? layers[layer_level + 1]
						: 0;

				neuralNetwork[layer_level][neuron_it] = new Neuron(layer_level, // current
																				// layer
																				// 0
																				// -
																				// 1
						neuron_it, // the index of the neuron
						number_of_weights, // how many weights are coming in
						number_of_previous_neurons, number_of_next_neurons);

				// Init previous Neurons - will not run for 1st layer
				for (int j = 0; j < number_of_previous_neurons; j++)
					neuralNetwork[layer_level][neuron_it].previous_layer[j] = neuralNetwork[layer_level - 1][j];
			}
		}

		// Initialize the next layer of the neurons
		for (int layerIt = 0; layerIt < layers.length; layerIt++) {
			for (int nodeIndexIt = 0; nodeIndexIt < layers[layerIt]; nodeIndexIt++) {
				int numOfNextNeurons = (layerIt < layers.length - 1) ? layers[layerIt + 1]
						: 0;
				for (int k = 0; k < numOfNextNeurons; k++)
					neuralNetwork[layerIt][nodeIndexIt].next_layer[k] = neuralNetwork[layerIt + 1][k];
			}
		}
	}// initialize

	// Training Procedure--------------------------------------------
	public static double useFileToTrain(int[] file, double gender) {

		//put the input at the bottom most layer
		for (int i = 0; i < SIZE_OF_PICTURE; i++) {
			for (int j = 0; j < layers[0]; j++) {
				neuralNetwork[0][j].incoming_input[i] = file[i];
			}
		}

		//get the outputs
		for (int i = 0; i < layers.length; i++) {
			for (int j = 0; j < layers[i]; j++) {
				neuralNetwork[i][j].setupInputs();
				neuralNetwork[i][j].calculateOutput();
			}
		}

		//deal with the errors
		neuralNetwork[layers.length - 1][0].delta = gender
				- neuralNetwork[layers.length - 1][0].output;
		
		for (int i = layers.length - 1; i >= 0; i--) {
			for (int j = 0; j < layers[i]; j++) {
				neuralNetwork[i][j].calculateError();
			}
		}

		//adjust everything accordingly
		for (int i = 0; i < layers.length; i++) {
			for (int j = 0; j < layers[i]; j++) {
				neuralNetwork[i][j].adjustWeights();
			}
		}

		return neuralNetwork[layers.length - 1][0].output;
	}

	public static void printWeights()
	{
		if (printWeightImages)
		{

			for (int i = 0; i < layers[0]; i++)
			{
				// neuralNetwork[0][i].incoming_weights[] is what we want
				BufferedImage imgOutput = new BufferedImage(128, 120, BufferedImage.TYPE_INT_RGB);	
				// this sucks, but we need to convert it to an int array. 
				int intArray[] = new int[SIZE_OF_PICTURE];

				int numPixels = 0;
				for (int y = 0; y < 120; y++)
					for (int x = 0; x < 128; x++)
					{
						int grayValue = Math.abs((int)neuralNetwork[0][i].incoming_weights[numPixels])*5;
						grayValue = (grayValue << 16) | (grayValue << 8) | grayValue;
						imgOutput.setRGB(x,y,grayValue);
						numPixels++;
					}


				String imageFileName = String.format("weights.%d.png", i);

				try {
					File imgOutputFile = new File(imageFileName);
					ImageIO.write(imgOutput, "png", imgOutputFile);
					System.out.format("Wrote file: weights.%d.png\r\n", i);
				}
				catch (IOException e)
				{
					System.out.format("Failed to write file: weights.%d.png\r\n", i);
				}

			}
		}
	}

	// Class that holds Neurons---------------------------------------------
	public static class Neuron {
		double[] incoming_input;
		double weighed_input = 0;
		double output = 0;
		double delta = 0;
		public double[] incoming_weights;
		public Neuron[] previous_layer;
		public Neuron[] next_layer;
		public int indexInNetwork;
		public int layer;
		public double w0 = 0.0;

		// Constructor------------------------------------------------------------
		Neuron(int layer, int index, int number_of_incoming_weights,
				int number_of_previous_neurons, int number_of_next_neurons) {

			this.layer = layer;
			this.indexInNetwork = index;// This is the amount of nodes in the
										// layer 5 - 1

			this.incoming_weights = new double[number_of_incoming_weights];
			this.incoming_input = new double[number_of_incoming_weights];

			this.previous_layer = new Neuron[number_of_previous_neurons];
			this.next_layer = new Neuron[number_of_next_neurons];

			initializeWeightsRandomly();
		}

		public void initializeWeightsRandomly() {
			// Initialize weights
			for (int i = 0; i < this.incoming_weights.length; i++){
				this.incoming_weights[i] = (Math.random() - 0.5) * 30.0;
				//this.incoming_weights[i] = this.incoming_weights[i] < 10 ?  6 : this.incoming_weights[i];
				//System.out.println("the weight is: " +this.incoming_weights[i] );
			}
		}

		public void setupInputs() {
			for (int i = 0; i < this.previous_layer.length; i++)
				this.incoming_input[i] = this.previous_layer[i].output;
		}

		public void calculateOutput() {
			this.weighed_input = this.w0;
			for (int i = 0; i < this.incoming_input.length; i++)
				this.weighed_input += this.incoming_input[i]
						* this.incoming_weights[i];
			this.output = this.sigmoidFunction(this.weighed_input);
		}

		public void calculateError() {
			// Can't do the error if it the next layer is nothing
			if (this.next_layer.length == 0)
				return;
			this.delta = 0;
			for (int i = 0; i < this.next_layer.length; i++) {
				// no use doing the error of the next neuron if this one
				// doesn't forward to anything
				if (this.next_layer[i] == null)
					continue;
				this.delta += this.next_layer[i].delta
						* this.next_layer[i].incoming_weights[this.indexInNetwork];
			}
		}

		public void adjustWeights() {
			double derivative = deriveSigmoid(this.weighed_input);
			double factor = LEARNING_STEP * this.delta * derivative;
			this.w0 += factor;
			for (int i = 0; i < this.incoming_weights.length; i++) {
				this.incoming_weights[i] =  derivative= this.incoming_weights[i]+
						factor * this.incoming_input[i];
				
				this.incoming_weights[i] = this.incoming_weights[i] > 10000? 10000 : 
					this.incoming_weights[i] < -10000? -10000 : this.incoming_weights[i];
			}
		}// adjust

		
		public double deriveSigmoid(double t) {
			double sigmoidValue = sigmoidFunction(t);
			// Hardcoded the derivative of sigmoid
			return sigmoidValue * (1 - sigmoidValue);
		}

		public double sigmoidFunction(double t) {
			double output = (1.0 / (1.0 + Math.exp(-t)));
			return output < 0.01? 0.01 : output > 0.99? 0.99 : output;
		}

	}

	// Class that holds info about a picture
	public static class Picture {
		public double gender;
		public String fileName;
		public int[] data;

		public Picture(String fileName, double gender) {
			this.gender = gender;
			this.fileName = fileName;
			this.data = readFileData(fileName);
			
		}

		public int[] readFileData(String fileName) {
			int[] data = new int[SIZE_OF_PICTURE];

			try {
				Scanner fileScanner = new Scanner(new File(fileName));
				for (int i = 0; i < SIZE_OF_PICTURE && fileScanner.hasNextInt(); i++)
					data[i] = fileScanner.nextInt();
				fileScanner.close();
			} catch (Exception e) {
				System.out.println("Could not find file: " + fileName);
				System.exit(0);
			}

			return data;
		}

	}
}
